//
//  MemoryGameView.swift
//  Memorize
//
//  Created by Franklin Byaruhanga on 20/05/2020.
//  Copyright © 2020 Franklin Byaruhanga. All rights reserved.
//

import SwiftUI

struct MemoryGameView: View {
    var viewModel: EmojiMemoryGame
    var body: some View {
        HStack {
            ForEach(self.viewModel.cards) { card in
                CardView(card: card).onTapGesture {
                    self.viewModel.choose(card: card)
                }
                    //Assignment 1: Task 3 set width to height ratio 2:3
                    .aspectRatio(0.67, contentMode: .fit)
            }
                //Assignment 1: Task 5 use smaller font if card pairs > 5
                .font(viewModel.cards.count > 8 ? .body : .largeTitle)
                .padding()
                .foregroundColor(Color.orange)
            
        }

    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MemoryGameView(viewModel: EmojiMemoryGame())
    }
}


