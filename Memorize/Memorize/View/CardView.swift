//
//  CardView.swift
//  Memorize
//
//  Created by Franklin Byaruhanga on 21/05/2020.
//  Copyright © 2020 Franklin Byaruhanga. All rights reserved.
//

import SwiftUI

struct CardView: View {
    var card:MemoryGame<String>.Card
    var body: some View {
        ZStack {
            if card.isFaceup {
                CardViewFront()
                Text(card.content)
            } else {
                CardViewBack()
            }
            
        }
    }
}

struct CardViewBack: View {
    var body: some View {
        RoundedRectangle(cornerRadius: 16)
            .fill(Color.orange)
            .overlay(
                RoundedRectangle(cornerRadius: 16)
                    .strokeBorder(Color.white,lineWidth: 2)
        )
    }
}

struct CardViewFront: View {
    var body: some View {
        RoundedRectangle(cornerRadius: 16)
            .fill(Color.white)
            .overlay(
                RoundedRectangle(cornerRadius: 16)
                    .strokeBorder(Color.orange,lineWidth: 2)
        )
    }
}
struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            CardView(card: EmojiMemoryGame().cards[0])
            CardViewBack()
                .previewLayout(.fixed(width: 140, height: 100))
            CardViewFront()
                .previewLayout(.fixed(width: 140, height: 100))
        }
    }
}

