//
//  MemoryGame.swift
//  Memorize
//
//  Created by Franklin Byaruhanga on 21/05/2020.
//  Copyright © 2020 Franklin Byaruhanga. All rights reserved.
//

import Foundation

struct MemoryGame<CardContent> {
    var cards:[Card] = []
    
    struct Card: Identifiable {
        var id: Int
        var isFaceup:Bool = true
        var isMatched:Bool = false
        var content:CardContent
    }
    
    func pick(card:Card) {
        print("Selected card \(card)")
    }
    
    init(numberofPairsOfCards:Int,cardContentFactory: (Int) -> CardContent) {
        self.cards = []
        for pairIndex in 0..<numberofPairsOfCards {
            let content = cardContentFactory(pairIndex)
            cards.append(Card(id: pairIndex*2, content: content))
            cards.append(Card(id: pairIndex*2+1, content: content))
        }
        
    }
}
