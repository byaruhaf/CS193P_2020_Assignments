//
//  EmojiMemoryGame.swift
//  Memorize
//
//  Created by Franklin Byaruhanga on 21/05/2020.
//  Copyright © 2020 Franklin Byaruhanga. All rights reserved.
//

import Foundation



class EmojiMemoryGame {
    private var model = EmojiMemoryGame.createMemoryGame()

    static func createMemoryGame() -> MemoryGame<String> {
        //Extra Credit: shuffle the Emoji's before they are selected.
        let emojis = ["🥰","😎","🥳","🥶","🤯","🤮","🤠","🤐","😹","🙎🏼‍♀️","👩🏻‍✈️","🤵🏽"].shuffled()
        //Assignment 1: Task 4 Random Pairs of Cards
        return .init(numberofPairsOfCards: Int.random(in: 2...5)) { emojis[$0] }
    }

    //Mark: - Access to the Model
    var cards:[MemoryGame<String>.Card] {
        //Assignment 1: Task 2 shuffle the cards
        return model.cards.shuffled()
    }

    //Mark: - Intent(s)
    func choose(card: MemoryGame<String>.Card) {
        model.pick(card: card)
    }


}
